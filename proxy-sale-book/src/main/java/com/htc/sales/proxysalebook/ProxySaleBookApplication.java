package com.htc.sales.proxysalebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableZuulProxy
@EnableSwagger2
public class ProxySaleBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProxySaleBookApplication.class, args);
	}

}

