package com.ht.sales.salebook.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ht.sales.salebook.dao.SaleDao;
import com.ht.sales.salebook.dao.impl.SaleDaoImpl;
import com.ht.sales.salebook.model.SaleModel;
import com.ht.sales.salebook.service.SaleService;

@Component
public class SaleServiceImpl implements SaleService {

	@Autowired
	SaleDao saledao;

	@Override
	public List<SaleModel> loadAllSales() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaleModel findSaleById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean modify(Integer id, SaleModel sale) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<SaleModel> findListById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insertBatch(List<SaleModel> sale) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean insert(SaleModel sale) {
		try {
				saledao.insert(sale);
				return Boolean.TRUE;
				
		} catch (Exception e) {
			e.getMessage();
		}
		return Boolean.FALSE;
	}

}
