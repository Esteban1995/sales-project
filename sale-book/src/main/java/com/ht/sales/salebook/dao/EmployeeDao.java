package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.EmployeeModel;

public interface EmployeeDao {

	List<EmployeeModel> loadAllEmployees();
}
