package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.EmployeeModel;

public interface EmployeeService {

	List<EmployeeModel> loadAllEmployees();
}
