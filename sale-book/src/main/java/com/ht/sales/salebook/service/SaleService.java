package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.SaleModel;

public interface SaleService {

	List<SaleModel> loadAllSales();

	SaleModel findSaleById(Integer id);

	boolean modify(Integer id, SaleModel sale);

	List<SaleModel> findListById(Integer id);

	boolean insertBatch(List<SaleModel> sale);
	
	boolean insert(SaleModel sale);
}
