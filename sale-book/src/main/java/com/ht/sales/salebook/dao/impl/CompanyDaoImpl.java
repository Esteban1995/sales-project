package com.ht.sales.salebook.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.ht.sales.salebook.dao.CompanyDao;
import com.ht.sales.salebook.model.CompanyModel;

public class CompanyDaoImpl extends JdbcDaoSupport implements CompanyDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<CompanyModel> loadAllCompany() {
		String sql = "SELECT * FROM company";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<CompanyModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			CompanyModel obj = new CompanyModel();
			obj.setIdcompany((Integer) row.get("idcompany"));
			obj.setName((String) row.get("name"));
			obj.setAddress((String) row.get("address"));
			obj.setMail((String) row.get("mail"));
			obj.setPhoneNumber((Integer) row.get("phonenumber"));
			result.add(obj);
		}

		return result;
	}

}
