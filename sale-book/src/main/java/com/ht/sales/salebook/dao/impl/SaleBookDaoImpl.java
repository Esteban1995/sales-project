package com.ht.sales.salebook.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.ht.sales.salebook.dao.SaleBookDao;
import com.ht.sales.salebook.model.SaleBookModel;

public class SaleBookDaoImpl extends JdbcDaoSupport implements SaleBookDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}
	
	@Override
	public boolean insert(SaleBookModel book) {
		String sql = "INSERT INTO salesbooks"
				+ "(iduser, idcompany, creationdate, year, month, subtotal, taxes, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql, new Object[] { book.getIdUser(), book.getIdCompany(), book.getCreationDate(),
				book.getYear(), book.getMonth(), book.getSubTotal(), book.getTaxes(), book.getTotal()

		});
		return Boolean.TRUE;
	}

	@Override
	public List<SaleBookModel> loadAllSaleBook() {
		String sql = "SELECT * FROM salesbooks";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<SaleBookModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			SaleBookModel obj = new SaleBookModel();
			obj.setIdBook((Integer) row.get("idbook"));
			obj.setIdUser((Integer) row.get("iduser"));
			obj.setIdCompany((Integer) row.get("idcompany"));
			obj.setCreationDate((Date) row.get("creationdate"));
			obj.setYear((Integer) row.get("year"));
			obj.setMonth((Integer) row.get("month"));
			obj.setSubTotal((BigDecimal) row.get("subtotal"));
			obj.setTaxes((BigDecimal) row.get("taxes"));
			obj.setTotal((BigDecimal) row.get("total"));
			result.add(obj);
		}

		return result;
	}

	@Override
	public SaleBookModel findSaleBookById(Integer id) {
		String sql = "SELECT * FROM salesbooks WHERE idbook= ?";
		List<SaleBookModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<SaleBookModel>() {
			@Override
			public SaleBookModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				SaleBookModel s = new SaleBookModel();
				s.setIdBook(rs.getInt("idbook"));
				s.setIdUser(rs.getInt("iduser"));
				s.setIdCompany(rs.getInt("idcompany"));
				s.setCreationDate(rs.getDate("creationdate"));
				s.setYear(rs.getInt("year"));
				s.setMonth(rs.getInt("month"));
				s.setSubTotal(rs.getBigDecimal("subtotal"));
				s.setTaxes(rs.getBigDecimal("taxes"));
				s.setTotal(rs.getBigDecimal("total"));
				return s;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public List<SaleBookModel> findListById(Integer id) {
		String sql = "SELECT * FROM salesbooks WHERE idbook =" + id;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<SaleBookModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			SaleBookModel list = new SaleBookModel();
			list.setIdBook((Integer) row.get("idbook"));
			list.setIdUser((Integer) row.get("iduser"));
			list.setIdCompany((Integer) row.get("icompany"));
			list.setCreationDate((Date) row.get("creationdate"));
			list.setYear((Integer) row.get("year"));
			list.setMonth((Integer) row.get("month"));
			list.setSubTotal((BigDecimal) row.get("subtotal"));
			list.setTaxes((BigDecimal) row.get("taxes"));
			list.setTotal((BigDecimal) row.get("total"));
			result.add(list);
		}
		return result;
	}

	@Override
	public Integer insertAndReturn(SaleBookModel book) {
		final String INSERT_SQL = "INSERT INTO salesbooks "
				+ "(iduser, idcompany, creationdate, year, month, subtotal, taxes, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERT_SQL, new String[] { "id_bill" });
				ps.setInt(1, book.getIdUser());
				ps.setInt(2, book.getIdCompany());
				ps.setDate(3, book.getCreationDate());
				ps.setInt(4, book.getYear());
				ps.setInt(5, book.getMonth());
				ps.setBigDecimal(6, book.getSubTotal());
				ps.setBigDecimal(7, book.getTaxes());
				ps.setBigDecimal(7, book.getTotal());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey();
	}

}
