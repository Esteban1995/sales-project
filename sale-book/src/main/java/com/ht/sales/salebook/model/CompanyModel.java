package com.ht.sales.salebook.model;

import java.io.Serializable;

public class CompanyModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idcompany;
	private String name;
	private String address;
	private String mail;
	private Integer phoneNumber;
	
	public CompanyModel() {
		super();
	}

	public CompanyModel(Integer idcompany, String name, String address, String mail, Integer phoneNumber) {
		super();
		this.idcompany = idcompany;
		this.name = name;
		this.address = address;
		this.mail = mail;
		this.phoneNumber = phoneNumber;
	}

	public Integer getIdcompany() {
		return idcompany;
	}

	public void setIdcompany(Integer idcompany) {
		this.idcompany = idcompany;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Company [idcompany=" + idcompany + ", name=" + name + ", address=" + address + ", mail=" + mail
				+ ", phoneNumber=" + phoneNumber + "]";
	}

}
