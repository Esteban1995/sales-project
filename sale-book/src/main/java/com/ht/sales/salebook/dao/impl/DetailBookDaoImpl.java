package com.ht.sales.salebook.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.ht.sales.salebook.dao.DetailBookDao;
import com.ht.sales.salebook.model.DetailBookModel;

public class DetailBookDaoImpl extends JdbcDaoSupport implements DetailBookDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<DetailBookModel> loadAllDetails() {
		String sql = "SELECT * FROM bookdetails";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<DetailBookModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			DetailBookModel obj = new DetailBookModel();
			obj.setIdBookDetail((Integer) row.get("id"));
			obj.setIdBook((Integer) row.get("idbook"));
			obj.setIdSale((Integer) row.get("idsale"));
			result.add(obj);
		}

		return result;
	}

	@Override
	public DetailBookModel findDetailById(Integer id) {
		String sql = "SELECT * FROM bookdetails WHERE id= ?";
		List<DetailBookModel> list = getJdbcTemplate().query(sql, new Object[] { id },
				new RowMapper<DetailBookModel>() {
					@Override
					public DetailBookModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
						DetailBookModel s = new DetailBookModel();
						s.setIdBookDetail(rs.getInt("id"));
						s.setIdBook(rs.getInt("idbook"));
						s.setIdSale(rs.getInt("idsale"));
						return s;
					}
				});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public List<DetailBookModel> findListById(Integer id) {
		String sql = "SELECT * FROM bookdetails WHERE idbook =" + id;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<DetailBookModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			DetailBookModel list = new DetailBookModel();
			list.setIdBookDetail((Integer) row.get("id"));
			list.setIdBook((Integer) row.get("idbook"));
			list.setIdSale((Integer) row.get("idsale"));
			result.add(list);
		}
		return result;
	}

	@Override
	public boolean insertBacth(List<DetailBookModel> detail) {
		String sql = "INSERT INTO bookdetails " + "(idbook, idsale) VALUES (?, ?)";
		Boolean result = false;
		try {
			getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					DetailBookModel d = detail.get(i);
					ps.setInt(1, d.getIdBook());
					ps.setInt(2, d.getIdSale());
				}

				public int getBatchSize() {
					return detail.size();
				}
			});
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
