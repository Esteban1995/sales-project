package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.DetailBookModel;

public interface DetailBookService {

	List<DetailBookModel> loadAllDetails();

	DetailBookModel findDetailById(Integer id);

	List<DetailBookModel> findListById(Integer id);
}
