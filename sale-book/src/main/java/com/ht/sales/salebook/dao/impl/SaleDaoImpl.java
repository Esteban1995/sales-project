package com.ht.sales.salebook.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.ht.sales.salebook.dao.SaleDao;
import com.ht.sales.salebook.model.SaleModel;

@Repository
public class SaleDaoImpl extends JdbcDaoSupport implements SaleDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(SaleModel sale) {
		String sql = "INSERT INTO sales "
				+ "(billnumber, idcompany, saledate, paydate, unpaid, printed, billtype, subtotal, subtotaliva, subtotalcesc, totaltaxes, total) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		getJdbcTemplate().update(sql,
				new Object[] { sale.getBillNumber(), sale.getIdCompany(), sale.getSaleDate(), sale.getPayDate(),
						sale.getUnpaid(), sale.getPrinted(), sale.getBillType(), sale.getSubTotal(),
						sale.getSubTotalIva(), sale.getSubTotalCesc(), sale.getSubTotalImp(), sale.getTotal()

				});
		return Boolean.TRUE;
	}

	@Override
	public List<SaleModel> loadAllSales() {
		String sql = "SELECT * FROM sales";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<SaleModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			SaleModel obj = new SaleModel();
			obj.setIdSale((Integer) row.get("idsale"));
			obj.setBillNumber((Integer) row.get("billnumber"));
			obj.setIdCompany((Integer) row.get("idcompany"));
			obj.setSaleDate((Date) row.get("saleDate"));
			obj.setPayDate((Date) row.get("payDate"));
			obj.setUnpaid((Boolean) row.get("unpaid"));
			obj.setPrinted((Boolean) row.get("printed"));
			obj.setBillType((String) row.get("billType"));
			obj.setSubTotal((Double) row.get("subtotal"));
			obj.setSubTotalIva((Double) row.get("subtotalIva"));
			obj.setSubTotalCesc((Double) row.get("subtotalCesc"));
			obj.setSubTotalImp((Double) row.get("subtotalTaxes"));
			obj.setTotal((Double) row.get("total"));
			result.add(obj);
		}

		return result;
	}

	@Override
	public SaleModel findSaleById(Integer id) {
		String sql = "SELECT * FROM sales WHERE idsale= ?";
		List<SaleModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<SaleModel>() {
			@Override
			public SaleModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				SaleModel s = new SaleModel();
				s.setIdSale(rs.getInt("idsale"));
				s.setBillNumber(rs.getInt("billnumber"));
				s.setIdCompany(rs.getInt("idcompany"));
				s.setSaleDate(rs.getDate("saleDate"));
				s.setPayDate(rs.getDate("payDate"));
				s.setUnpaid(rs.getBoolean("unpaid"));
				s.setPrinted(rs.getBoolean("printed"));
				s.setBillType(rs.getString("billType"));
				s.setSubTotal(rs.getDouble("subtotal"));
				s.setSubTotalIva(rs.getDouble("subtotalIva"));
				s.setSubTotalCesc(rs.getDouble("subtotalCesc"));
				s.setSubTotalImp(rs.getDouble("subtotalTaxes"));
				s.setTotal(rs.getDouble("total"));
				return s;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public boolean modify(Integer id, SaleModel sale) {
		String sql = "UPDATE sales SET payDate = ? WHERE idsale =" + id;
		getJdbcTemplate().update(sql, new Object[] { sale.getPayDate() });
		return true;
	}

	@Override
	public List<SaleModel> findListById(Integer id) {
		String sql = "SELECT * FROM sales WHERE idsale =" + id;
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<SaleModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			SaleModel obj = new SaleModel();
			obj.setIdSale((Integer) row.get("idsale"));
			obj.setBillNumber((Integer) row.get("billnumber"));
			obj.setIdCompany((Integer) row.get("idcompany"));
			obj.setSaleDate((Date) row.get("saleDate"));
			obj.setPayDate((Date) row.get("payDate"));
			obj.setUnpaid((Boolean) row.get("unpaid"));
			obj.setPrinted((Boolean) row.get("printed"));
			obj.setBillType((String) row.get("billType"));
			obj.setSubTotal((Double) row.get("subtotal"));
			obj.setSubTotalIva((Double) row.get("subtotalIva"));
			obj.setSubTotalCesc((Double) row.get("subtotalCesc"));
			obj.setSubTotalImp((Double) row.get("subtotalTaxes"));
			obj.setTotal((Double) row.get("total"));
			result.add(obj);
		}
		return result;
	}

	@Override
	public boolean insertBacth(List<SaleModel> sale) {
		String sql = "INSERT INTO sales "
				+ "(billnumber, idcompany, saleDate, payDate, unpaid, printed, billType, subtotal, subtotalIva, subtotalCesc, totalTaxes, total) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		Boolean result = false;
		try {
			getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {

					SaleModel s = sale.get(i);
					ps.setInt(1, s.getIdSale());
					ps.setInt(2, s.getBillNumber());
					ps.setInt(3, s.getIdCompany());
					ps.setDate(4, (Date) s.getSaleDate());
					ps.setDate(5, (Date) s.getPayDate());
					ps.setBoolean(6, s.getUnpaid());
					ps.setBoolean(7, s.getPrinted());
					ps.setString(8, s.getBillType());
					ps.setDouble(9, s.getSubTotal());
					ps.setDouble(10, s.getSubTotalIva());
					ps.setDouble(11, s.getSubTotalCesc());
					ps.setDouble(12, s.getSubTotalImp());
					ps.setDouble(13, s.getTotal());
				}

				public int getBatchSize() {
					return sale.size();
				}
			});
			result = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

}
