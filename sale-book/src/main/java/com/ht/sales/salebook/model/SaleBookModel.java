package com.ht.sales.salebook.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class SaleBookModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idBook;
	private Integer idUser;
	private Integer idCompany;
	private Date creationDate;
	private Integer month;
	private Integer year;
	private BigDecimal subTotal;
	private BigDecimal taxes;
	private BigDecimal total;
	
	public SaleBookModel() {
		
	}

	public SaleBookModel(Integer idBook, Integer idUser, Integer idCompany, Date creationDate, Integer month,
			Integer year, BigDecimal subTotal, BigDecimal taxes, BigDecimal total) {
		super();
		this.idBook = idBook;
		this.idUser = idUser;
		this.idCompany = idCompany;
		this.creationDate = creationDate;
		this.month = month;
		this.year = year;
		this.subTotal = subTotal;
		this.taxes = taxes;
		this.total = total;
	}

	public Integer getIdBook() {
		return idBook;
	}

	public void setIdBook(Integer idBook) {
		this.idBook = idBook;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getTaxes() {
		return taxes;
	}

	public void setTaxes(BigDecimal taxes) {
		this.taxes = taxes;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "SaleBookModel [idBook=" + idBook + ", idUser=" + idUser + ", idCompany=" + idCompany + ", creationDate="
				+ creationDate + ", month=" + month + ", year=" + year + ", subTotal=" + subTotal + ", taxes=" + taxes
				+ ", total=" + total + "]";
	}
	
		

}
