package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.UserModel;

public interface UserDao {

	List<UserModel> loadAllUsers();

	UserModel findUsersById(Integer id);
}
