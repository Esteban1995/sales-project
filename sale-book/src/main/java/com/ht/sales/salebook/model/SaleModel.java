package com.ht.sales.salebook.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

public class SaleModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idSale;
	private Integer billNumber;
	private Integer idCompany;
	private Date saledate;
	private Date payDate;
	private Boolean unpaid;
	private Boolean printed;
	private String billType;
	private Double subTotal;
	private Double subTotalIva;
	private Double subTotalCesc;
	private Double subTotalImp;
	private Double total;
	
	public SaleModel() {
		
	}

	public SaleModel(Integer idSale, Integer billNumber, Integer idCompany, Date saledate, Date payDate, Boolean unpaid,
			Boolean printed, String billType, Double subTotal, Double subTotalIva, Double subTotalCesc,
			Double subTotalImp, Double total) {
		super();
		this.idSale = idSale;
		this.billNumber = billNumber;
		this.idCompany= idCompany;
		this.saledate = saledate;
		this.payDate = payDate;
		this.unpaid = unpaid;
		this.printed = printed;
		this.billType = billType;
		this.subTotal = subTotal;
		this.subTotalIva = subTotalIva;
		this.subTotalCesc = subTotalCesc;
		this.subTotalImp = subTotalImp;
		this.total = total;
	}

	public Integer getIdSale() {
		return idSale;
	}

	public void setIdSale(Integer idSale) {
		this.idSale = idSale;
	}

	public Integer getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(Integer billNumber) {
		this.billNumber = billNumber;
	}

	public Integer getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(Integer idCompany) {
		this.idCompany = idCompany;
	}

	public Date getSaleDate() {
		return saledate;
	}

	public void setSaleDate(Date saledate) {
		this.saledate = saledate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public Boolean getUnpaid() {
		return unpaid;
	}

	public void setUnpaid(Boolean unpaid) {
		this.unpaid = unpaid;
	}

	public Boolean getPrinted() {
		return printed;
	}

	public void setPrinted(Boolean printed) {
		this.printed = printed;
	}

	public String getBillType() {
		return billType;
	}

	public void setBillType(String billType) {
		this.billType = billType;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getSubTotalIva() {
		return subTotalIva;
	}

	public void setSubTotalIva(Double subTotalIva) {
		this.subTotalIva = subTotalIva;
	}

	public Double getSubTotalCesc() {
		return subTotalCesc;
	}

	public void setSubTotalCesc(Double subTotalCesc) {
		this.subTotalCesc = subTotalCesc;
	}

	public Double getSubTotalImp() {
		return subTotalImp;
	}

	public void setSubTotalImp(Double subTotalImp) {
		this.subTotalImp = subTotalImp;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "SaleModel [idSale=" + idSale + ", billNumber=" + billNumber + ", saleDate=" + saledate
				+ ", payDate=" + payDate + ", unpaid=" + unpaid + ", printed=" + printed + ", billType=" + billType
				+ ", subTotal=" + subTotal + ", subTotalIva=" + subTotalIva + ", subTotalCesc=" + subTotalCesc
				+ ", subTotalImp=" + subTotalImp + ", total=" + total + "]";
	}	
}