package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.SaleModel;

public interface SaleDao {

	boolean insert(SaleModel sale);

	List<SaleModel> loadAllSales();

	SaleModel findSaleById(Integer id);

	boolean modify(Integer id, SaleModel sale);

	List<SaleModel> findListById(Integer id);

	boolean insertBacth(List<SaleModel> sale);
}
