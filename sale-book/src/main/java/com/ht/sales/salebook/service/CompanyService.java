package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.CompanyModel;

public interface CompanyService {

	List<CompanyModel> loadAllCompany();
}
