package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.UserModel;

public interface UserService {

	List<UserModel> loadAllUsers();

	UserModel findUsersById(Integer id);
}
