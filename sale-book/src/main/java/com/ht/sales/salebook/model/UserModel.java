package com.ht.sales.salebook.model;

import java.io.Serializable;

public class UserModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idUser;
	private String username;
	private Integer idEmployee;
	
	public UserModel() {
		super();
	}

	public UserModel(Integer idUser, String username, Integer idEmployee) {
		super();
		this.idUser = idUser;
		this.username = username;
		this.idEmployee = idEmployee;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	@Override
	public String toString() {
		return "Users [idUser=" + idUser + ", username=" + username + ", idEmployee=" + idEmployee + "]";
	}

}
