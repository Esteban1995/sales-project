package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.DetailBookModel;

public interface DetailBookDao {

	List<DetailBookModel> loadAllDetails();

	DetailBookModel findDetailById(Integer id);

	List<DetailBookModel> findListById(Integer id);
	
	public boolean insertBacth(List<DetailBookModel> detail);

}
