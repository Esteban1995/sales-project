package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.CompanyModel;

public interface CompanyDao {

	List<CompanyModel> loadAllCompany();
}
