package com.ht.sales.salebook.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.ht.sales.salebook.dao.UserDao;
import com.ht.sales.salebook.model.UserModel;

public class UserDaoImpl extends JdbcDaoSupport implements UserDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<UserModel> loadAllUsers() {
		String sql = "SELECT * FROM company";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<UserModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			UserModel obj = new UserModel();
			obj.setIdEmployee((Integer) row.get("iduser"));
			obj.setUsername((String) row.get("username"));
			obj.setIdEmployee((Integer) row.get("idemployee"));
			result.add(obj);
		}

		return result;
	}

	@Override
	public UserModel findUsersById(Integer id) {
		String sql = "SELECT * FROM users WHERE idemployee= ?";
		List<UserModel> list = getJdbcTemplate().query(sql, new Object[] { id }, new RowMapper<UserModel>() {
			@Override
			public UserModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				UserModel e = new UserModel();
				e.setIdUser(rs.getInt("iduser"));
				e.setUsername(rs.getString("username"));
				e.setIdEmployee(rs.getInt("idemployee"));
				return e;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

}
