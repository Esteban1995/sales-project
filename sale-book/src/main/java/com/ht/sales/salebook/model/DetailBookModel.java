package com.ht.sales.salebook.model;

import java.io.Serializable;

public class DetailBookModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idBookDetail;
	private Integer idBook;
	private Integer idSale;
	
	public DetailBookModel() {
		
	}

	public DetailBookModel(Integer idBookDetail, Integer idBook, Integer idSale) {
		super();
		this.idBookDetail = idBookDetail;
		this.idBook = idBook;
		this.idSale = idSale;
	}

	public Integer getIdBookDetail() {
		return idBookDetail;
	}

	public void setIdBookDetail(Integer idBookDetail) {
		this.idBookDetail = idBookDetail;
	}

	public Integer getIdBook() {
		return idBook;
	}

	public void setIdBook(Integer idBook) {
		this.idBook = idBook;
	}

	public Integer getIdSale() {
		return idSale;
	}

	public void setIdSale(Integer idSale) {
		this.idSale = idSale;
	}

	@Override
	public String toString() {
		return "SaleModel [idBookDetail=" + idBookDetail + ", idBook=" + idBook + ", idSale=" + idSale + "]";
	}
	
	

}

	