package com.ht.sales.salebook.dao;

import java.util.List;

import com.ht.sales.salebook.model.SaleBookModel;

public interface SaleBookDao {

	boolean insert(SaleBookModel book);

	List<SaleBookModel> loadAllSaleBook();

	SaleBookModel findSaleBookById(Integer id);

	List<SaleBookModel> findListById(Integer id);
	
	public Integer insertAndReturn(SaleBookModel book);
}
