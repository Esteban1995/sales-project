package com.ht.sales.salebook.model;

import java.io.Serializable;

public class EmployeeModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idEmployee;
	private String name;
	private String lastname;
	private Integer phoneNumber;
	private String address;
	
	public EmployeeModel() {
		super();
	}

	public EmployeeModel(Integer idEmployee, String name, String lastname, Integer phoneNumber, String address) {
		super();
		this.idEmployee = idEmployee;
		this.name = name;
		this.lastname = lastname;
		this.phoneNumber = phoneNumber;
		this.address = address;
	}

	public Integer getIdEmployee() {
		return idEmployee;
	}

	public void setIdEmployee(Integer idEmployee) {
		this.idEmployee = idEmployee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employee [idEmployee=" + idEmployee + ", name=" + name + ", lastname=" + lastname + ", phoneNumber="
				+ phoneNumber + ", address=" + address + "]";
	}
	
}
