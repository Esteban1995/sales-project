package com.ht.sales.salebook.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.ht.sales.salebook.dao.EmployeeDao;
import com.ht.sales.salebook.model.EmployeeModel;

public class EmployeeDaoImpl extends JdbcDaoSupport implements EmployeeDao {

	@Autowired
	DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public List<EmployeeModel> loadAllEmployees() {
		String sql = "SELECT * FROM employees";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<EmployeeModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			EmployeeModel obj = new EmployeeModel();
			obj.setIdEmployee((Integer) row.get("idemployee"));
			obj.setName((String) row.get("name"));
			obj.setLastname((String) row.get("lastname"));
			obj.setPhoneNumber((Integer) row.get("phonenumber"));
			obj.setAddress((String) row.get("address"));
			result.add(obj);
		}

		return result;
	}

}
