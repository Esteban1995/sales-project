package com.ht.sales.salebook.service;

import java.util.List;

import com.ht.sales.salebook.model.SaleBookModel;

public interface SaleBookService {

	boolean insert(SaleBookModel book);

	List<SaleBookModel> loadAllSaleBook();

	SaleBookModel findSaleBookById(Integer id);

	List<SaleBookModel> findListById(Integer id);
}
