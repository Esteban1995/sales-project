package com.ht.sales.salebook;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ht.sales.salebook.model.SaleModel;
import com.ht.sales.salebook.service.SaleService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SaleTest {

	@Autowired
	private SaleService saleService;

	@Test
	public void test() {
		java.util.Date fecha = new java.util.Date();
		java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
		
		SaleModel sale = new SaleModel();
		sale.setBillNumber(20);
		sale.setIdCompany(1);
		sale.setSaleDate(fechasql);
		sale.setPayDate(fechasql);
		sale.setUnpaid(false);
		sale.setPrinted(true);
		sale.setBillType("FC");
		sale.setSubTotal(22.22);
		sale.setSubTotalIva(3.65);
		sale.setSubTotalCesc(1.05);
		sale.setSubTotalImp(4.70);
		sale.setTotal(25.00);
		
		assertEquals(true, saleService.insert(sale));
		
	}

}
