package com.htc.sales.gatewaysalebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GatewaySaleBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewaySaleBookApplication.class, args);
	}

}

