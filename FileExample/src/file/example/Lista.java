package file.example;

import java.util.Date;
import java.util.List;

public class Lista {

	private String fecha;
	private Integer id;
	private Integer factura;
	private List<Lista> lista3;
	
	

	public Lista() {
		
	}

	public Lista(String fecha, Integer id, Integer factura) {
		super();
		this.fecha = fecha;
		this.id = id;
		this.factura = factura;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFactura() {
		return factura;
	}

	public void setFactura(Integer factura) {
		this.factura = factura;
	}
	
	public List<Lista> getLista3() {
		return lista3;
	}

	public void setLista3(List<Lista> lista3) {
		this.lista3 = lista3;
	}

	@Override
	public String toString() {
		return "Lista [fecha=" + fecha + ", id=" + id + ", factura=" + factura + "]";
	}
	
	
}
